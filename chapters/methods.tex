\section{Methodology}

The presented approach has the goal to classify images of bee wings into one of 124 species and to determine which features contributed the most to the prediction of a given individual. To obtain this goal, the approach is structured into three parts: (1) computing the relevance map, which contains the information about the relevance of each pixel, (2) extracting morphological features (veins, cells, junctions) from the semantic segmentation output and (3) fusing the relevance information with the obtained morphological features to create a ranking.

\subsection{Relevance map calculation}

\subsubsection{Visual Backpropagation}
\label{sec:vbp}
Visual Backpropagation is an efficient method to visualize sets of input pixels that contribute the most to an output prediction \cite{VisualBackProp}. It produces visualization masks that are similar to those of the state-of-the-art method LRP, but is significantly faster. The method works by merging later feature maps, which are small in size and contain dense, relevant information, with feature maps from earlier layers, which contain less relevant information but have a higher resolution. Feature maps near the input layer have dimensions close to the input image for which the visualization mask is produced, but often only encode information about simple shapes that are not necessarily task-specific \cite{YosinskiCBL14}. In contrast, feature maps near the output layer have a low spatial resolution, but they encode relevant information obtained from the forward pass needed to classify the input. On their own, outputs from deep and shallow layers cannot be used to determine \emph{which} input neurons (pixels) contribute to \emph{which} class prediction. 

VBP uses backpropagation to combine class information from deep with spatial information from shallow layers, but back-propagate activations instead of gradients.
\Cref{fig:vbp} illustrates the ''visual backpropagation''. The values that are back-propagated to shallow layers (i.e. multiplied with them) are feature maps, or activations of the layer's neurons. 
%Because a layer $ L_i $ usually computes $ N $ feature maps that can be different to the $ K $ feature maps of another layer $ L_j $, feature maps of a layer are averaged at first.
Each layer's feature maps are averaged at first. Then, to be able to element-wise multiply the feature map of $ L_i $ with the average feature map of layer $ L_{i-1} $, they must have the same spatial dimensions. The averaged $ L_i $ feature map is therefore upsampled to the size of the averaged $ L_{i-1} $ feature map by deconvolution. Here, weights are set to 1 and biases to 0. Kernel size and padding are calculated to ensure that width and height match after deconvolution. The resulting matrix of the multiplication is again scaled up and multiplied by the next average feature map, until the input image is reached. After the process is finished, the result is a visualization mask that is normalized to the range $ [0, 1] $. This process is described with the formula

\begin{align}
	\phi_{VBP}(X) = \gamma(X) \sum_{P \in \mathcal{P}} \prod_{e \in P} a_e
\end{align}

where $ \gamma(X) $ the value of an input pixel $ X $, $ \mathcal{P} $ a family of paths from $ X $ to the output layer and $ a_e $ the neuron activation along edge $ e $. The summation of all paths leading to the $ F $ different feature maps of a given layer corresponds to the averaging of its feature maps described above (with a factor of $ \frac{1}{F} $). The product of activations along a path corresponds to the element-wise multiplications described above. There exists a universal constant $ c > 0 $ such that $ c \cdot \phi_{VBP}(X) $ is an efficient approximation of the true contribution $ \phi(X) $ of neuron $ X $,

\begin{align}
	\phi(X) = \gamma(X) \sum_{P \in \mathcal{P}} \prod_{e \in P} \frac{a_e}{a_e + b_e} w_e
\end{align}

with $ b_e $ being the bias and $ w_e $ being the weight along edge $ e $. The authors remark that for relatively small kernel sizes, the number of paths $ |\mathcal{P}| $ is small, therefore the difference between the approximation $ \phi_{VBP}(X) $ and $ \phi(X) $, which is defined by the factor $ \frac{w_e}{a_e + b_e} $, becomes small as well. See \cite{VisualBackProp} for the theoretical derivation and proofs of the formulas. Finally, because the calculation is applied for all input neurons in the same way and we are only interested in their relative contribution, we do not need to find the optimal constant $ c $. Instead, we set $ c $ to $ \frac{1}{\sum_{x \in I} \phi_{VBP}(x)} $ to normalize the result.

The operations used in the Visual Backpropagation algorithm are averaging, deconvolution, and element-wise multiplication. As such, it can be implemented directly in the architecture of a CNN by using mean, deconvolution and multiplication layers.

\begin{figure}[ht!]
	\includegraphics[width=\textwidth]{img/figure-vbp}
	\caption{Calculation of a visualization mask using the Visual Backpropagation algorithm \cite{VisualBackProp}.}
	\label{fig:vbp}
\end{figure}

\subsubsection{Equipping DeepABIS with Visual Backpropagation}

DeepABIS utilizes the lightweight and efficient MobileNetV2 convolutional neural network \cite{Sandler2018}. This architecture introduces depth-wise separable convolutional layers combined with inverted residuals. Specifically, it contains multiple blocks (called ''linear bottlenecks'') consisting of an expansion layer, a depth-wise separable convolutional layer, and a projection layer. Expansion and projection layers are convolutional layers with a $ 1 \times 1 $ spatial kernel size. Between input and output of a block exists a residual connection similar to ResNet \cite{HeZRS15}. A single bottleneck block is intended to replace a regular convolutional layer with a faster and more fittable operation by using depth-wise separable convolution and residuals. Therefore, in the context of Visual Backpropagation, a block is regarded as one convolutional layer, with the output of the projection layer serving as the feature maps to be averaged. This also requires fewer multiplications to be conducted.

Instead of applying the Visual Backpropagation algorithm directly, the algorithm is incorporated into the model architecture itself in form of mean, deconvolution and multiplication layers. In doing so, the approach is more easily shippable (as a .h5 file, for instance) to other environments as it does not require porting the algorithm code. The attachment procedure is outlined in \cref{algo:attach}. To equip the MobileNetV2 architecture with the Visual Backpropagation method, each layer of size $ W \times H \times N $ is appended with a layer that computes the mean along the feature map axis $ N $. To the last (deepest) mean layer at position $ |L| $, a deconvolution layer is attached. This in turn gets connected to a multiplication layer together with the mean layer of $ |L-1| $. To its result is again attached a deconvolutional layer, which is itself multiplied with mean layer $ |L-2| $, and so on. When the input layer is reached, the last attached layer (the deconvolution of the last multiplication) is the layer producing the resulting visualization mask of VBP. With the Visual Backpropagation process attached to the model, it from now on generates visualization masks in the same forward pass as classifying the input image. For a given input neuron $ x $ (or point/pixel in an image) of the input layer $ I $, we then get the corresponding normalized visualization mask, or ''relevance map''

\begin{align}
	 R_I(x) = \frac{\phi_{VBP}(x)}{\sum_{z \in I} \phi_{VBP}(z)}.
\end{align}

\begin{figure}[h]
\begin{algorithm}[H]
\caption{Attach VBP to a model.} 
\label{algo:attach}
\begin{algorithmic}

\Function{AttachVBP}{$model,L$}
\State $ attach \gets None $
\ForAll{$ layer \in \text{reverse}(L) $}
	\State $ average \gets \text{Mean}(axis=3)(layer) $
	\State $ up\_shape \gets \text{shape}(\text{prev}(layer)) $
	\State $ stride \gets up\_shape / shape(layer) $
	\If{$ attach $ is $ None $}
		\State $ attach \gets \text{Deconv}(1, \text{kernel}(layer), stride, \text{padding}(layer))(average) $
	\Else
		\State $ multiply \gets \text{Multiply}()(average, attach) $
		\If{$ layer $ is last}
			\State $ attach \gets \text{Deconv}(1, \text{kernel}(layer), stride, \text{padding}(layer))(multiply) $
		\Else
			\State $ attach \gets multiply $
		\EndIf
	\EndIf
\EndFor
\State Attach $ attach $ to output of $ model $
\EndFunction

\end{algorithmic}

\end{algorithm}
\caption{Algorithm to attach the VBP method to a given model. Note that $ L $ is traversed in reverse, from the deepest to the shallowest layer of the network.}
\end{figure}

\subsection{Feature Segmentation}

The next step involves extracting segments from the input image which can serve as morphological features. These are then ranked later in the fusion with the method outlined in \cref{sec:vbp}. Semantic segmentation makes predictions for every pixel and labels it with the class of its enclosing segment. It can be approached similarly to pattern classification using convolutional neural networks. Past attempts at this topic have shown robust, outstanding results \cite{RonnebergerFB15, BadrinarayananK15, HeGDG17, LongSD14, Yu2016}. The general architecture consists of an encoder-decoder structure, with the encoder performing a classification of the image to infer semantic information and the decoder projecting the learned discriminative features onto the output pixel space. For DeepABIS, a MobileNetV2 model \cite{Sandler2018} serves as the encoder to meet the same requirements, namely a fast forward pass, low memory usage and high classification performance. For the decoder part, the pixel-wise U-Net \cite{RonnebergerFB15} architecture is used. It was created for the segmentation of biomedical images, specifically microscopic imagery depicting cells, which is similar to the use case of DeepABIS where microscopic imagery is used as well. The combination of a MobileNet\footnote{MobileNetV1.} \cite{Howard2017} encoder with a U-Net meta-architecture was shown to provide accurate segmentation results compared to Dilation \cite{Yu2016} and SkipNet \cite{LongSD14} in a comparative study \cite{Siam2018ACS}.
% TODO: train and test on another decoder network (Enet?)
% TODO: diagram of architecture

\begin{figure*}[ht!]
	\centering
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/input-65}
		\caption{}
		\label{fig:input}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/seg-65}
		\caption{}
		\label{fig:seg}
    \end{subfigure}%
    ~\\
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/thresh-65}
		\caption{}
		\label{fig:thresh}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/labels-65}
		\caption{}
		\label{fig:label}
    \end{subfigure}%
    \caption{Cell segmentation process. First, the encoder-decoder network outputs the segmentation result (\ref{fig:seg}). The result is thresholded (\ref{fig:thresh}) and subsequently labeled (\ref{fig:label}).}
    \label{fig:segmentation}
\end{figure*}

Instead of providing each cell and each vein segment separately however, the resulting encoder-decoder network outputs only two segments, a ''veins'' and a ''cells'' segment. They are provided using the annotation described in \cref{sec:data}. Therefore, to obtain individual cells of a wing image, the cells segment is first thresholded and then each sub-segment labeled by connected-component labeling (cf. \cref{fig:segmentation}). For the veins after thresholding the result, the binary mask is skeletonized using Zhang's algorithm \cite{Zhang1984}. For this, \emph{skeletonize} of the \emph{skimage} package is used. Finally, to obtain individual veins and  junctions, an undirected, planar graph $ G = (V, E) $ is generated from the skeletonized binary mask by creating nodes $ V $ from junctions and creating edges $ E $ from tracing the connections between junctions (cf. \cref{fig:seg-vein}). This is done by \emph{sknw} of Yan Xiaolong\footnote{https://github.com/yxdragon/sknw}. Having transformed the semantic segmentation result into a graph model of morphological features, we can detect and discard features that we deem irrelevant to our modeled problem. Specifically, as we are only interested in the central wing venation, all edges and nodes of the graph that are not part of the central wing venation are removed. This can be accomplished by removing all edges that are not part of a cycle, as veins not in the central venation move outwards and do not continue, either by getting weaker and invisible (therefore not being able to form a closed path) or moving out of the image boundaries. 

The resulting graph $ G' $ only contains nodes that are part of a simple cycle, i.e. they enclose a face in a planar graph (cf. \cref{fig:cycle}), in this context a cell. Theses cycles form a minimum cycle basis $ C $ for $ G' $ (a minimal list of cycles such that any cycle in the graph can be expressed by the sum of cycles in the basis). We further construct a graph of cycles $ G_C = (C, E_C) $ from this cycle basis of $ G' $ by creating a node for each cycle $ c \in C $. There exists an edge $ \{c, d\} \in E_C $ if $ c \cap d \neq \emptyset $, i.e. there exists an edge between two nodes in $ G_C $ if there exists a node in $ G' $ which is an element of both cycle $ c $ and cycle $ d $. In other words, a new graph is created from $ G' $ where each minimal cycle (or cell in this context) is a node, and two cycles are connected to each other if they are neighbors, meaning they share at least one node in the original graph (cf. \cref{fig:planar}). See \cref{fig:graphs} for a visualization. In each node in $ G_C $, we store the cell found by connected component labeling to which the node/cycle/face belongs.

Using the cycle graph $ G_C $, the nomenclature outlined in \cref{sec:data} can be applied to the extracted cells. To assign each cell/node the corresponding zoological label, a rule-based neighborhood approach is utilized. We assume to have two types of venation structures: (i) one which has exactly two, and (ii) one which has exactly three C-cells (cubital cells). Based on this assumption, a search for the B-cell (brachial cell) is conducted at first. We define a B-cell as a cell with exactly two neighbors (discoidal cells, D1 and D2). These neighbors are labeled D-cells. The C-cells are found by labeling all unlabeled neighbors of the D-cells as C-cells. Finally, the remaining cells are labeled R-cells (radial cells). To solve the ambiguity in type (i) where both the B- and the R-cell have exactly two neighbors (the D-cells for B and the C-cells for R), we additionally define the B-cell to always be vertically below the R-cell. This assumes that we know the orientation of the wing image. An example of a labeled graph is shown in \cref{fig:label-graph}. The labels are then assigned to the segmented cells through the information stored in each $ G_C $ node (cf. \cref{fig:matched}).

As a result, we have extracted the set of morphological features $ F $ we want to rank: each cell and its respective name, as well as each individual vein and junction by its respective edge and node in $ G' $.

\begin{figure*}[ht!]
	\centering
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/veins-thresh-65}
		\caption{}
		\label{fig:veins}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/skeleton-65}
		\caption{}
		\label{fig:skeleton}
    \end{subfigure}%
    ~\\
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/graph-input-65}
		\caption{}
		\label{fig:graph-unfiltered}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/graph-final-65}
		\caption{}
		\label{fig:graph}
    \end{subfigure}%
    \caption{Vein segmentation process. First, the vein segment is thresholded (\ref{fig:veins}). Then, the mask is skeletonized (\ref{fig:skeleton}). An undirected graph is generated from the skeletonization (\ref{fig:graph-unfiltered}). This graph is then trimmed by removing all edges that are not part of any cycle (\ref{fig:graph}).}
    \label{fig:seg-vein}
\end{figure*}

\begin{figure*}[ht!]
	\centering
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/graph-65}
		\caption{Planar graph $ G' $ generated from the skeleton mask.}
		\label{fig:cycle}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.5\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/graph-planar-65}
		\caption{Graph $ G_C $ created from the cycle basis of $ G' $. Each face of $ G' $ corresponds to one node.}
		\label{fig:planar}
    \end{subfigure}%
    \caption{}
    \label{fig:graphs}
\end{figure*}

\begin{figure*}[ht!]
	\begin{subfigure}[t]{0.6\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/graph-planar-labels-65}
		\caption{$ G_C $ labeled according to the nomenclature.}
		\label{fig:label-graph}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{img/cells-labels-65}
		\caption{Visualization of the cells and their respective zoological labels obtained from the cycle graph.}
		\label{fig:matched}	
   	 \end{subfigure}%
   	 \caption{}
\end{figure*}


\subsection{Fusion}

For the last step, the result of the pixel-wise relevance map generation and the feature segmentation step are combined. The segments extracted by the encoder-decoder network are infused with semantic information about their importance to the classification result.
Each feature $ i \in F $ (junction, vein or cell) receives a relevance score $ r: i \rightarrow [0,1] $. The score is defined as the mean contribution $ r(i) $ of a feature pixel to the classification, with

\begin{align}
	r(i) = \frac{1}{|P_i|} \sum_{p \in P_i} R_I(p)
\end{align}

where $ P_i $ is the set of points belonging to feature $ i $, obtained in the segmentation step, and $ R_I $ the relevance map obtained in the Visual Backpropagation process. The score is calculated for all features, i.e. the set of cells $ C $, the set of veins $ V $ and the set of junctions $ J $. After calculating the scores of every feature, the features can be ranked in descending order, ranging from highest contribution at the top to lowest contribution at the bottom. Additionally, the percentage $ h(i) \in [0,1] $ of feature $ i $

\begin{align}
	h(i) = \frac{r(i)}{\sum_{j \in C \cup V \cup J} r(j)}
\end{align}

is calculated to show the contribution of each feature relative to the contribution of all segmented features.
Thus, by combining information about the features themselves (their points and their meaning, i.e. their zoological label) taken from the segmentation step, with information about the contribution of each point to the inference result, we obtain a ranking of all abstract, semantic features according to their relative importance to the classification.