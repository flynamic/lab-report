\section{Experiments}
First, some results of the visual backpropagation step are presented. They are collected from four samples taken from the test dataset, from four different species and genera: \emph{apis mellifera}, \emph{bombus magnus}, \emph{lasioglossum leucozonium} and \emph{osmia californica}. Visualizations of the relevance maps are shown in \cref{fig:results-vbp} in the described order. As can be seen in each map, pixels in the wing venation are the brightest parts of the picture, while the background and some veins are partly or completely dark. Looking closely at the visualizations, one can observe checkerboard artifacts over the images. These arise due to deconvolution in VBP operating on kernel sizes that are not divisible by the stride. Although the goal is visualizing features extracted by the segmentation model and not displaying the visualizations shown here, this could potentially lead to problems when calculating the score for each segment, since it will incorrectly have pixels with a low $ R $ value that are surrounded by pixels with a high value, which should not make sense. However, this issue exists for almost all segments equally, so the influence on the score calculation is assumed to be small. This problem can be solved in several ways, for example by separating the deconvolution layer into an upsampling and a convolution step. Nonetheless, from what can be observed, the VBP subnet is working correctly and producing pixel maps which indicate the approximate contribution of a pixel to the final classification result.

\begin{figure*}[ht!]
	\centering
	\tiny
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/65}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/vbp-apis-newer}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/bombus}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/vbp-bombus-new}
    \end{subfigure}%
	~\\
	\begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/4417}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/vbp-4417}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/osmia}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/vbp-osmia}
    \end{subfigure}%
    \caption{Visualizations of $ R $-maps produced by the attached visual backpropagation subnet of the DeepABIS model.}
    \label{fig:results-vbp}
\end{figure*}
~\\
Next, the model for the segmentation task, MobileNetV2+UNet, is evaluated.
Training was conducted for 60 epochs with weights pre-trained on ImageNet.
% TODO: weights pre-trained on ABIS
Input and output image size are thus $ 224 \times 224 $. The input data was standardized by subtracting the mean and scaling to unit variance. Categorical cross-entropy was used as loss function and Adam served as optimization algorithm \cite{Kingma2015Adam}. Testing took place on a subset of the DeepABIS dataset, comprising 760 samples or 10\% of the total dataset size.
\Cref{fig:accuracy} displays the progression of the pixel-wise categorical accuracy during training. The accuracy metric reaches 93\% after 21 epochs and consecutively converges to approximately 93\%. To measure the spatial accuracy of the detected segments, the Intersection-over-Union metric is depicted in red. IoU converges after roughly 30 epochs as well, moving around the range between 0.85 and 0.9.

\begin{figure*}[ht!]
	\begin{tikzpicture}
		\begin{axis}[
					width=\linewidth, % Scale the plot to \linewidth
		          grid=major, % Display a grid
		          grid style={dashed,gray!30}, % Set the style
		          xlabel=Epoch, % Set the labels
		          ylabel=Accuracy/IoU,
		          legend style={at={(0.95, 0.05)},anchor=south east}, % Put the legend below the plot
		          ]
			\addplot +[mark=none] table [x=Step, y=Value, col sep=comma] {data/run_2019-02-23_18-08-00-tag-epoch_val_categorical_accuracy.csv};
			\addlegendentry{Accuracy}
			\addplot +[mark=none] table [x=Step, y=Value, col sep=comma] {data/run_2019-05-26_19-47-00-tag-epoch_val_iou_score.csv};
			\addlegendentry{IoU}
		\end{axis}
	\end{tikzpicture}
  \caption[Top-1 accuracy curves]{Pixel-wise categorical accuracy of the MobileNetV2+UNet model (classes ''background'', ''veins'', ''cells''), and the Intersection-over-Union (IoU) value over time.}
  \label{fig:accuracy}
\end{figure*}

Finally, we evaluate the finished feature ranking method on the test data. Various exemplary test images are shown in \cref{fig:ranking}, together with their top-3 features, including the relative scores $ h(i) $. Each test image shown here was correctly classified by the VBP-extended classification model. It can be observed that for different species, often the same features receive the highest relevance score. Particularly the R- and C-cells seem to carry discriminative visual cues for the classifier to differentiate between species, which is why the pixels associated with them contribute the most to the classification result. For \emph{apis}, at least for these few test images $ C_1 $ appears to carry the most relevant information, while for the other depicted species R- cells take on this role. However, the relative scores indicate that there are no features which could predict the result single-handedly, as every cell, vein or junction in these examples has a low $ h(i) $ value (here always $ < 0.08 $). This suggests that the classification decision strongly depends on the interplay of multiple features at once. Another unexpected observation is that for some species, a part of the main vein at the top plays a significant role in the classification (to be seen for the \emph{lasioglossum} and \emph{bombus} genera). They are mistakenly labeled as (separate) Radial cells, because the segmentation model classified these regions as ''cell''. We further produced a ranking where only veins were considered as morphological features (depicted in \cref{fig:veins-ranking}) as they seem to receive most of the relevance judging from the visualization maps.

\begin{figure*}[ht!]
	\centering
	\tiny
	% Apis
    Apis mellifera
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/65}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/rank-65-1}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/rank-65-2}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/rank-65-3}
    \end{subfigure}%
    ~\\
    % Apis ligustica
    Apis ligustica
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/apis-ligustica}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/rank-apis-ligustica-1}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/rank-apis-ligustica-2}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/rank-apis-ligustica-3}
    \end{subfigure}%
    ~\\
    % Bombus
    Bombus magnus
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/bombus}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/rank-bombus-1}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/rank-bombus-2}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/rank-bombus-3}
    \end{subfigure}%
    ~\\
    % Lasioglossum
    Lasioglossum leucozonium
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/4417}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/rank-4417-1}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/rank-4417-2}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/rank-4417-3}
    \end{subfigure}%
    ~\\
    % Osmia
    Osmia californica
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/osmia}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/rank-osmia-1}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/rank-osmia-2}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/rank-osmia-3}
    \end{subfigure}%
    \caption{Feature ranking. Depicted are (from left to right): original wing images and their three highest-ranking features with their $h$-values.}
    \label{fig:ranking}
\end{figure*}

\begin{figure*}[ht!]
	\centering
	\tiny
	% Apis
    Apis mellifera
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/65}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/veins/rank-veins-apis-mellifera-1}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/veins/rank-veins-apis-mellifera-2}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/veins/rank-veins-apis-mellifera-3}
    \end{subfigure}%
    ~\\
    % Apis ligustica
    Apis ligustica
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/apis-ligustica}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/veins/rank-veins-apis-ligustica-1}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/veins/rank-veins-apis-ligustica-2}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/veins/rank-veins-apis-ligustica-3}
    \end{subfigure}%
    ~\\
    % Bombus
    Bombus magnus
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/bombus}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/veins/rank-veins-bombus-magnus-1}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/veins/rank-veins-bombus-magnus-2}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/veins/rank-veins-bombus-magnus-3}
    \end{subfigure}%
    ~\\
    % Lasioglossum
    Lasioglossum leucozonium
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/4417}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/veins/rank-veins-lasioglossum-leucozonium-1}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/veins/rank-veins-lasioglossum-leucozonium-2}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/veins/rank-veins-lasioglossum-leucozonium-3}
    \end{subfigure}%
    ~\\
    % Osmia
    Osmia californica
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/osmia}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/veins/rank-veins-osmia-californica-1}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/veins/rank-veins-osmia-californica-2}
    \end{subfigure}%
    ~
    \begin{subfigure}[t]{0.25\textwidth}
        \centering
		\includegraphics[width=\textwidth]{img/experiments/veins/rank-veins-osmia-californica-3}
    \end{subfigure}%
    \caption{Rankings of the same specimen as in \cref{fig:ranking}, but with veins being the only morphological features analyzed.}
    \label{fig:veins-ranking}
\end{figure*}